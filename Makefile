PREFIX ?= /data/data/hilled.pwnterm/files/usr

all:
	@echo Run \'make install\' to install Base.

install:
	@mkdir -p $(PREFIX)/etc
	@cp -p os-release $(PREFIX)/etc/os-release
	@cp -p bash.motd $(PREFIX)/etc/bash.motd
	@cp -p pfetch $(PREFIX)/bin/pfetch
	@chmod +x $(PREFIX)/bin/pfetch
	@mkdir -p $(PREFIX)/home
	@touch $(PREFIX)/home/.home_done

uninstall:
	@rm $(PREFIX)/etc/os-release
	@rm $(PREFIX)/etc/bash.motd
	@rm $(PREFIX)/bin/pfetch
	@rm $(PREFIX)/home/.home_done

